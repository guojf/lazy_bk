package com.dolphin.scheduled;

import com.dolphin.commons.ScheduledTaskAnnotation;
import com.dolphin.service.DataBaseBackupService;
import com.dolphin.service.ScheduledTaskJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-18 13:06
 * 个人博客地址：https://www.nonelonely.com
 */
@ScheduledTaskAnnotation(initStartFlag = 1, taskDesc = "数据库备份", taskCron = "0 0 1 * * ?")
public class DataBaseBackupJob implements ScheduledTaskJob {
    private final Logger log = LoggerFactory.getLogger(DataBaseBackupJob.class);
    @Autowired
    private DataBaseBackupService dataBaseBackupService;

    @Override
    public void run() {
        try {
            dataBaseBackupService.mysqlBackups();
        } catch (Exception e) {
            log.error("数据库备份执行失败{}", e);
        }
    }

}
