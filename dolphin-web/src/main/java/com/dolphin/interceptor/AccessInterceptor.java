package com.dolphin.interceptor;


import com.dolphin.model.AccessLogs;
import com.dolphin.service.AccessLogsService;
import eu.bitwalker.useragentutils.UserAgent;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;


/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-07 20:20
 * 个人博客地址：https://www.nonelonely.com
 */
public class AccessInterceptor implements HandlerInterceptor {
    private final Logger logger = LoggerFactory.getLogger(AccessInterceptor.class);

    @Autowired
    private AccessLogsService accessLogsService;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        try {
            CacheManager cacheManager = CacheManager.getInstance();
            Ehcache cache = cacheManager.getEhcache("access_logs");
            if (cache == null) {
                CacheConfiguration cacheConfiguration = new CacheConfiguration();
                cacheConfiguration.timeToIdleSeconds(60 * 10);
                cacheConfiguration.setName("access_logs");
                cacheConfiguration.eternal(false);
                cacheConfiguration.timeToLiveSeconds(60 * 10);
                cacheConfiguration.setMaxBytesLocalHeap("50M");
                Cache accessLogsCache = new Cache(cacheConfiguration);
                cacheManager.addCache(accessLogsCache);
                addCache(cacheManager.getEhcache("access_logs"), request);
            } else {
                addCache(cache, request);
            }
        } catch (Exception e) {
            logger.error("访问失败{0}", e);
        }
    }

    public void addCache(Ehcache cache, HttpServletRequest request) {
        Element element = cache.get(getIpAddress(request));
        if (element == null) {
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
            AccessLogs accessLogs = new AccessLogs();
            accessLogs.setBrowserGroup(userAgent.getBrowser().getGroup().toString());
            accessLogs.setBrowserName(userAgent.getBrowser().getName());
            accessLogs.setBrowserVersion(userAgent.getBrowserVersion().getVersion());

            accessLogs.setIp(getIpAddress(request));
            accessLogs.setSystemGroup(userAgent.getOperatingSystem().getGroup().toString());
            accessLogs.setSystemInfo(userAgent.getOperatingSystem().getName());
            accessLogs.setSystemType(userAgent.getOperatingSystem().getDeviceType().toString());
            accessLogs.setDate(new Date());
            accessLogsService.addAccess(accessLogs);
            cache.put(new Element(getIpAddress(request), accessLogs));
        }
    }

    private String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-requested-For");
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip != null && ip.contains(",")) {
            String[] ips = ip.split(",");
            for (int index = 0; index < ips.length; ++index) {
                String strIp = ips[index];
                if (!"unknown".equalsIgnoreCase(strIp)) {
                    ip = strIp;
                    break;
                }
            }
        }
        return ip;
    }
}
