package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.monitor.Server;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.ArticleService;
import com.dolphin.service.CommentService;
import com.dolphin.service.TagService;
import com.dolphin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class DashboardController extends BaseController {
    @Value("${version}")
    private String version;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private TagService tagService;

    @Autowired
    private UserService userService;

    @RequestMapping("/dashboard")
    @ApiAuth(name = "控制台", permission = "admin:dashboard:index")
    public String index(Model model) throws Exception {
        model.addAttribute("articleCount", articleService.getArticleCount());
        model.addAttribute("commentCount", commentService.getCommentCount());
        model.addAttribute("tagCount", tagService.getTagCount());
        model.addAttribute("userCount", userService.getUserCount());
        model.addAttribute("version", version);
        Server server = new Server();
        server.copyTo();
        model.addAttribute("server", server);

        return view("static/admin/pages/dashboard/dashboard.html");
    }

    /**
     * 获取最新文章列表
     *
     * @return ResponseBean
     */
    @GetMapping("/dashboard/getArticleList")
    @ResponseBody
    public ResponseBean getArticleList(int count) {
        return ResponseBean.success("获取成功", articleService.getArticleListByDashboard(count));
    }


    /**
     * 获取最新评论列表
     *
     * @return ResponseBean
     */
    @GetMapping("/dashboard/getCommentList")
    @ResponseBody
    public ResponseBean getCommentList(int count) {
        return ResponseBean.success("获取成功", commentService.getCommentListByDashboard(count));
    }
}
