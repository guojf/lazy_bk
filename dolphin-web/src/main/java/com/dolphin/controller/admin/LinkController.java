package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Link;
import com.dolphin.model.Permission;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.LinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin")
public class LinkController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(LinkController.class);

    @Autowired
    private LinkService linkService;

    /**
     * 友链列表页
     *
     * @return String
     */
    @RequestMapping("/link")
    @ApiAuth(name = "友链管理", permission = "admin:link:index", groupId = Constants.ADMIN_MENU_GROUP_CONTENT)
    public String index() {
        return view("static/admin/pages/link/link_list.html");
    }

    /**
     * 友链管理列表数据
     *
     * @return String
     */
    @PostMapping("/link/list")
    @ResponseBody
    @ApiAuth(name = "友链管理列表数据", permission = "admin:link:list")
    public Pager<Link> list(@RequestBody Pager<Link> pager) {
        return linkService.list(pager);
    }

    /**
     * 友链添加页
     *
     * @return String
     */
    @RequestMapping("/link/addPage")
    @ApiAuth(name = "友链添加页", permission = "admin:link:addPage",type = Permission.ResType.NAV_LINK)
    public String addPage() {
        return view("static/admin/pages/link/link_add.html");
    }

    /**
     * 添加友链
     *
     * @return String
     */
    @PostMapping("/link/add")
    @ResponseBody
    @ApiAuth(name = "添加友链", permission = "admin:link:add")
    public ResponseBean add(@RequestBody Link link) {
        if (linkService.add(link) > 0) {
            return ResponseBean.success("添加成功", link);
        }
        logger.error("友链添加失败: {}", link.toString());
        return ResponseBean.fail("添加失败", null);
    }

    /**
     * 友链编辑页
     *
     * @return String
     */
    @GetMapping("/link/editPage/{id}")
    @ApiAuth(name = "友链编辑页", permission = "admin:link:editPage",type = Permission.ResType.NAV_LINK)
    public String editPage(@PathVariable("id") String id, Model model) {
        Link link = linkService.getById(id);
        model.addAttribute("link", link);
        return view("static/admin/pages/link/link_edit.html");
    }

    /**
     * 更新友链
     *
     * @return String
     */
    @PostMapping("/link/update")
    @ResponseBody
    @ApiAuth(name = "更新友链", permission = "admin:link:add")
    public ResponseBean update(@RequestBody Link link) {
        if (linkService.update(link) > 0) {
            return ResponseBean.success("更新成功", null);
        }
        logger.error("友链更新失败: {}", link.toString());
        return ResponseBean.fail("更新失败", null);
    }

    /**
     * 删除友链
     *
     * @return String
     */
    @PostMapping("/link/del")
    @ResponseBody
    @ApiAuth(name = "删除友链", permission = "admin:link:del")
    public ResponseBean del(@RequestBody String ids) {
        String[] idArr = ids.split(",");
        if (linkService.del(idArr) > 0) {
            return ResponseBean.success("删除成功", null);
        }
        logger.error("友链删除失败: {}", ids);
        return ResponseBean.fail("删除失败", null);
    }
}
