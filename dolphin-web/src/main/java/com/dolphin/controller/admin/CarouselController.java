package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.model.Carousel;
import com.dolphin.model.Permission;
import com.dolphin.permission.AdminMenu;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.CarouselService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-13 22:15
 * 个人博客地址：https://www.nonelonely.com
 */
@Controller
@RequestMapping("/admin")
public class CarouselController extends BaseController {

    @Autowired
    private CarouselService carouselService;

    /**
     * @return java.lang.String
     * @description 轮播管理页
     * @author dolphin
     */
    @GetMapping("/carousel")
    @ApiAuth(name = "轮播管理",groupId = Constants.ADMIN_MENU_GROUP_CONTENT, permission = "admin:carousel:index")
    public String index() {
        return "/static/admin/pages/carousel/list.html";
    }

    /**
     * @return java.lang.String
     * @description 添加轮播
     * @author dolphin
     */
    @GetMapping("/carousel/addPage")
    @ApiAuth(name = "添加轮播",type = Permission.ResType.NAV_LINK, permission = "admin:carousel:addPage")
    public String addCarouselPage() {
        return "/static/admin/pages/carousel/add.html";
    }

    /**
     * 编辑轮播
     *
     * @return String
     */
    @GetMapping("/carousel/editPage/{id}")
    @ApiAuth(name = "编辑轮播",type = Permission.ResType.NAV_LINK, permission = "admin:carousel:editPage")
    public String editPage(@PathVariable("id") String id, Model model) {
        Carousel carousel = carouselService.getById(Long.parseLong(id));
        model.addAttribute("carousel", carousel);
        return view("/static/admin/pages/carousel/edit.html");
    }
}
