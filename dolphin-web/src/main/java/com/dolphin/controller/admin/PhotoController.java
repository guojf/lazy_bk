package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.model.Permission;
import com.dolphin.model.Photo;
import com.dolphin.model.Photos;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.PhotoService;
import com.dolphin.service.PhotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class PhotoController extends BaseController {

    @Autowired
    private PhotosService photosService;

    @Autowired
    private PhotoService photoService;

    @RequestMapping("/photo/listPage/{id}")
    @ApiAuth(name = "照片管理", permission = "admin:photo:photo",type = Permission.ResType.NAV_LINK)
    public String listPage(@PathVariable("id") String id, Model model) {
        Photos photos = photosService.getById(Long.parseLong(id));
        model.addAttribute("photos", photos);
        return "/static/admin/pages/photo/photo-list.html";
    }

    @RequestMapping("/photo/addPage/{id}")
    @ApiAuth(name = "添加照片管理", permission = "admin:photo:addPage",type = Permission.ResType.NAV_LINK)
    public String addPage(@PathVariable("id") String id, Model model) {
        model.addAttribute("photosId", id);
        return "/static/admin/pages/photo/photo-add.html";
    }

    /**
     * 编辑图片页
     *
     * @return String
     */
    @GetMapping("/photo/editPage/{id}")
    @ApiAuth(name = "编辑照片管理", permission = "admin:photo:editPage",type = Permission.ResType.NAV_LINK)
    public String editPage(@PathVariable("id") String id, Model model) {
        Photo photo = photoService.getById(Long.parseLong(id));
        model.addAttribute("photo", photo);
        return view("/static/admin/pages/photo/photo-edit.html");
    }

}
