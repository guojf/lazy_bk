package com.dolphin.controller.admin;

import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.DynamicDataSource;
import com.dolphin.config.SwaggerProperties;
import com.dolphin.permission.ApiAuth;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AboutController extends BaseController {

    @Value("${version}")
    private String version;
    @Autowired
    private SwaggerProperties swaggerProperties;

    @GetMapping("/about")
    @ApiAuth(name = "打开关于系统", permission = "admin:about:index", groupId = Constants.ADMIN_MENU_GROUP_SETTING)
    public String index(Model model) {
        model.addAttribute("version", version);
        model.addAttribute("dataBase", DynamicDataSource.dataSourceType);
        model.addAttribute("javaVersion", System.getProperty("java.version"));
        model.addAttribute("osName", System.getProperty("os.name"));
        model.addAttribute("osArch", System.getProperty("os.arch"));
        model.addAttribute("swaggerEnable", swaggerProperties.getEnable() ? "是" : "否");
        return view("static/admin/pages/about/about.html");
    }
}
