package com.dolphin.controller.api;

import com.dolphin.base.BaseApiController;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Photo;
import com.dolphin.service.PhotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@Api(value = "相册图片相关", tags = "相册图片相关")
@RequestMapping("/api/photo")
public class PhotoController extends BaseApiController {


    @Autowired
    private PhotoService photoService;

    /**
     * 添加图片
     *
     * @return String
     */
    @PostMapping("/addPhoto")
    @ApiOperation(value = "添加图片", notes = "添加图片")
    public ResponseBean addPhotos(@RequestBody @Valid Photo photo) {
        try {
            photo.setUserId(getUser().getId());
            photoService.addPhoto(photo);
            return ResponseBean.success("添加成功", photo);
        } catch (Exception e) {
            return ResponseBean.fail("添加失败", e.getMessage());
        }
    }

    /**
     * @param pager pager
     * @return com.dolphin.commons.Pager<com.photos.model.Photos>
     * @description 图片分页
     * @author dolphin
     */
    @RequestMapping("/list")
    @ApiOperation(value = "获取图片分页", notes = "获取图片分页")
    public Pager<Photo> list(@RequestBody Pager<Photo> pager) {
        return photoService.list(pager);
    }

    /**
     * 删除图片
     *
     * @return String
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除图片", notes = "删除图片")
    public ResponseBean del(@RequestBody String id) {
        if (photoService.del(id) > 0) {
            return ResponseBean.success("删除成功", null);
        }
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更新图片
     *
     * @return String
     */
    @PostMapping("/editPhoto")
    @ApiOperation(value = "更新图片", notes = "删除图片")
    public ResponseBean editPhoto(@RequestBody @Valid Photo photo) {
        if (photoService.update(photo) > 0) {
            return ResponseBean.success("更新成功", null);
        }
        return ResponseBean.fail("更新失败", null);
    }
}
