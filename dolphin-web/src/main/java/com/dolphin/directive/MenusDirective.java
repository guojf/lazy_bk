package com.dolphin.directive;

import cn.hutool.core.collection.CollectionUtil;
import com.dolphin.commons.Constants;
import com.dolphin.commons.OptionCacheUtil;
import com.dolphin.model.Menu;
import com.dolphin.service.MenuService;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * menus模板指令
 */
@TemplateDirective("menus")
@Component
public class MenusDirective extends BaseDirective {

    private static MenuService menuService;

    @Autowired
    public void setMenuService(MenuService menuService) {
        MenusDirective.menuService = menuService;
    }

    public void setExprList(ExprList exprList) {
        super.setExprList(exprList);
    }

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        String url = OptionCacheUtil.getValue(Constants.OPTION_WEB_THEME);
        List<Menu> menus = menuService.getProtalMenus(url);
        if (CollectionUtil.isEmpty(menus)) {
            menus = new ArrayList<>();
            scope.set("menus", menus);
        } else {
            scope.set("menus", menus.get(0).getChildMenu());
        }
        stat.exec(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
