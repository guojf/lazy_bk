package com.dolphin.directive;

import com.dolphin.model.Article;
import com.dolphin.model.User;
import com.dolphin.service.ArticleService;
import com.dolphin.service.UserService;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-09 21:45
 * 个人博客地址：https://www.nonelonely.com
 */
@TemplateDirective("latestUser")
@Component
public class LatestUserDirective extends BaseDirective{

    private static UserService userService;

    @Autowired
    public void setArticleService(UserService userService){
        LatestUserDirective.userService = userService;
    }

    public void setExprList(ExprList exprList) {
        super.setExprList(exprList);
    }

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        HashMap<String, String> para = exprListToMap();
        int count = Integer.parseInt(para.get("count"));
        List<User> latestUser = userService.getLatestUser(count);
        scope.set("users", latestUser);
        stat.exec(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
