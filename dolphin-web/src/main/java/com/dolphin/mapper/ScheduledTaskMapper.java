package com.dolphin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dolphin.model.ScheduledTask;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-06 23:04
 * 个人博客地址：https://www.nonelonely.com
 */
@Mapper
@Component
public interface ScheduledTaskMapper extends BaseMapper<ScheduledTask> {
}
