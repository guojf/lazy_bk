package com.dolphin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dolphin.model.Word;
import com.dtflys.forest.annotation.Query;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-24 22:21
 * 个人博客地址：https://www.nonelonely.com
 */
@Mapper
@Component
public interface WordMapper extends BaseMapper<Word> {
    Word findRandWords(int limit);
}
