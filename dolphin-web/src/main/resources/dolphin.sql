/*
Navicat MySQL Data Transfer

Source Server         : 华为
Source Server Version : 50651
Source Host           : 121.37.140.240:3306
Source Database       : dolphin

Target Server Type    : MYSQL
Target Server Version : 50651
File Encoding         : 65001

Date: 2022-10-16 13:54:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for d_backups
-- ----------------------------
DROP TABLE IF EXISTS `d_backups`;
CREATE TABLE `d_backups` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `mysql_ip` varchar(15) DEFAULT NULL COMMENT '数据库IP',
  `mysql_port` varchar(5) DEFAULT NULL COMMENT '数据库端口',
  `mysql_cmd` varchar(230) DEFAULT NULL COMMENT '备份命令',
  `mysql_back_cmd` varchar(230) DEFAULT NULL COMMENT '恢复命令',
  `database_name` varchar(20) DEFAULT NULL COMMENT '数据库名称',
  `backups_path` varchar(50) DEFAULT NULL COMMENT '备份数据地址',
  `backups_name` varchar(50) DEFAULT NULL COMMENT '备份文件名称',
  `operation` int(11) DEFAULT NULL COMMENT '操作次数',
  `status` int(1) DEFAULT NULL COMMENT '数据状态（1正常，-1删除）',
  `recovery_time` datetime DEFAULT NULL COMMENT '恢复时间',
  `create_time` datetime DEFAULT NULL COMMENT '备份时间',
  PRIMARY KEY (`id`),
  KEY `baskups_index` (`mysql_ip`,`mysql_port`,`backups_path`,`database_name`,`backups_name`) USING BTREE COMMENT '索引'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='MySQL数据备份表';

-- ----------------------------
-- Table structure for p_access_logs
-- ----------------------------
DROP TABLE IF EXISTS `p_access_logs`;
CREATE TABLE `p_access_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `system_info` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统信息',
  `system_group` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统家族',
  `system_type` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统类型',
  `browser_version` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '浏览器版本',
  `browser_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '浏览器名称',
  `browser_group` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '浏览器家族',
  `ip` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'ip',
  `date` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for p_article
-- ----------------------------
DROP TABLE IF EXISTS `p_article`;
CREATE TABLE `p_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL COMMENT '文章标题',
  `content` longtext COMMENT '文章内容',
  `contentModel` varchar(32) DEFAULT NULL COMMENT '文章内容类型:html/markdown',
  `source_type` int(11) DEFAULT '0' COMMENT '文章来源类型（0原创 1网上）',
  `type` varchar(32) DEFAULT NULL COMMENT '文章类型:article文章,page页面',
  `summary` varchar(1024) DEFAULT NULL COMMENT '文章摘要',
  `categoryId` int(11) DEFAULT NULL COMMENT '所属分类',
  `metaKeywords` varchar(512) DEFAULT NULL COMMENT 'SEO关键字',
  `metaDescription` varchar(512) DEFAULT NULL COMMENT 'SEO描述',
  `thumbnail` varchar(256) DEFAULT NULL COMMENT '缩略图',
  `slug` varchar(128) DEFAULT NULL COMMENT 'slug',
  `isTop` int(11) DEFAULT '0' COMMENT '是否置顶0:否,1:是',
  `status` int(11) DEFAULT '0' COMMENT '状态0:已发布,1:草稿',
  `commentCount` int(11) DEFAULT '0' COMMENT '评论数',
  `viewCount` int(11) DEFAULT '0' COMMENT '访问量',
  `greatCount` int(11) DEFAULT '0' COMMENT '访问量',
  `userId` int(11) NOT NULL COMMENT '创建人',
  `isComment` int(11) DEFAULT '1' COMMENT '是否允许评论0:否,1是',
  `flag` varchar(256) DEFAULT NULL COMMENT '标识',
  `template` varchar(256) DEFAULT NULL COMMENT '模板',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `slug` (`slug`),
  KEY `isTop` (`isTop`),
  KEY `type` (`type`),
  KEY `categoryId` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `p_article_tag`;
CREATE TABLE `p_article_tag` (
  `articleId` int(11) NOT NULL COMMENT '文章id',
  `tagId` int(11) NOT NULL COMMENT '标签id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_attach
-- ----------------------------
DROP TABLE IF EXISTS `p_attach`;
CREATE TABLE `p_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(256) NOT NULL COMMENT '附件名',
  `desc` varchar(512) DEFAULT NULL COMMENT '附件描述',
  `path` varchar(512) NOT NULL COMMENT '附件路径',
  `suffix` varchar(32) DEFAULT NULL COMMENT '附件后缀',
  `flag` varchar(256) DEFAULT NULL COMMENT '标识',
  `type` varchar(32) DEFAULT NULL COMMENT '文件类型',
  `saveType` varchar(32) NOT NULL COMMENT '存储方式',
  `fileKey` varchar(512) NOT NULL COMMENT 'fileKey',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`),
  KEY `saveType` (`saveType`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_carousel
-- ----------------------------
DROP TABLE IF EXISTS `p_carousel`;
CREATE TABLE `p_carousel` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '轮播图标题',
  `image_Url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '轮播图图片路径',
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '轮播图跳转路径',
  `over_Time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '轮播图过期时间',
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '轮播图备注',
  `create_Time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_Time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` int(11) DEFAULT NULL COMMENT '用户id',
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for p_category
-- ----------------------------
DROP TABLE IF EXISTS `p_category`;
CREATE TABLE `p_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(256) NOT NULL COMMENT '分类名',
  `pid` int(11) NOT NULL DEFAULT '-1' COMMENT '父级id',
  `desc` varchar(512) DEFAULT NULL COMMENT '描述',
  `count` int(11) NOT NULL DEFAULT '0' COMMENT '文章数量',
  `metaKeywords` varchar(256) DEFAULT NULL COMMENT 'SEO关键字',
  `thumbnail` varchar(256) DEFAULT NULL COMMENT '封面图',
  `slug` varchar(128) DEFAULT NULL COMMENT 'slug',
  `metaDescription` varchar(256) DEFAULT NULL COMMENT 'SEO描述内容',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态0:正常,1禁用',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status` (`status`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='分类表';

-- ----------------------------
-- Table structure for p_comment
-- ----------------------------
DROP TABLE IF EXISTS `p_comment`;
CREATE TABLE `p_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `articleId` int(11) NOT NULL COMMENT '文章id',
  `pid` int(11) DEFAULT '-1' COMMENT '父级id',
  `topPid` int(11) DEFAULT '-1' COMMENT '顶层父级id',
  `userId` int(11) DEFAULT NULL COMMENT '用户iD',
  `content` varchar(2048) DEFAULT NULL COMMENT '评论内容',
  `status` int(11) DEFAULT '0' COMMENT '状态:0正常,1:待审核',
  `avatar` varchar(256) DEFAULT NULL COMMENT '头像',
  `website` varchar(256) DEFAULT NULL COMMENT '网站地址',
  `email` varchar(256) NOT NULL COMMENT '邮箱',
  `userName` varchar(256) NOT NULL COMMENT '评论人',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `articleId` (`articleId`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_link
-- ----------------------------
DROP TABLE IF EXISTS `p_link`;
CREATE TABLE `p_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(256) NOT NULL COMMENT '网站名',
  `logo` varchar(256) DEFAULT NULL COMMENT '网站logo',
  `desc` varchar(512) DEFAULT NULL COMMENT '网站描述',
  `address` varchar(256) NOT NULL COMMENT '网站地址',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_menu
-- ----------------------------
DROP TABLE IF EXISTS `p_menu`;
CREATE TABLE `p_menu` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `pid` varchar(64) DEFAULT '-1' COMMENT '父级id',
  `name` varchar(128) NOT NULL COMMENT '菜单名',
  `url` varchar(128) DEFAULT NULL COMMENT '菜单链接',
  `icon` varchar(64) DEFAULT NULL COMMENT '菜单图标',
  `seq` int(11) DEFAULT NULL COMMENT '排序序号',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '菜单类型0:前台,1:后台',
  `target` int(11) DEFAULT '0' COMMENT '菜单打开方式:0本页,1:新窗口',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '菜单状态0:启用,1禁用',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `theme` varchar(255) DEFAULT NULL COMMENT '属于主题',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_option
-- ----------------------------
DROP TABLE IF EXISTS `p_option`;
CREATE TABLE `p_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `option_key` varchar(256) NOT NULL COMMENT 'key',
  `option_value` text COMMENT 'value',
  `remark` varchar(256) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_permission
-- ----------------------------
DROP TABLE IF EXISTS `p_permission`;
CREATE TABLE `p_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '链接地址',
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '名称',
  `permission_value` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限',
  `permission_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限类型',
  `enable` int(11) DEFAULT NULL COMMENT '是否启用',
  `create_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for p_photo
-- ----------------------------
DROP TABLE IF EXISTS `p_photo`;
CREATE TABLE `p_photo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `photosId` int(11) NOT NULL COMMENT '相册id',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `desc` varchar(64) DEFAULT NULL COMMENT '描述',
  `url` varchar(64) DEFAULT NULL COMMENT 'url',
  `userId` int(11) NOT NULL COMMENT '用户id',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_photos
-- ----------------------------
DROP TABLE IF EXISTS `p_photos`;
CREATE TABLE `p_photos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(256) DEFAULT NULL COMMENT '相册名',
  `desc` varchar(1024) DEFAULT NULL COMMENT '描述',
  `coverUrl` varchar(128) DEFAULT NULL COMMENT '封面url',
  `isEncryption` int(11) DEFAULT '0' COMMENT '是否加密:0正常,1:加密',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `userId` int(11) NOT NULL COMMENT '用户id',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_plugin
-- ----------------------------
DROP TABLE IF EXISTS `p_plugin`;
CREATE TABLE `p_plugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(256) DEFAULT NULL COMMENT '插件名',
  `path` varchar(256) NOT NULL COMMENT '路径',
  `desc` varchar(512) DEFAULT NULL COMMENT '插件描述',
  `version` varchar(64) DEFAULT NULL COMMENT '版本',
  `author` varchar(64) DEFAULT NULL COMMENT '作者',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '插件状态:0禁用,1启用',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_role
-- ----------------------------
DROP TABLE IF EXISTS `p_role`;
CREATE TABLE `p_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) NOT NULL COMMENT '角色名',
  `description` varchar(256) DEFAULT NULL COMMENT '角色描述',
  `code` varchar(32) NOT NULL COMMENT '角色码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `p_role_menu`;
CREATE TABLE `p_role_menu` (
  `roleId` int(11) NOT NULL COMMENT '角色id',
  `menuId` varchar(64) NOT NULL COMMENT '菜单id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `p_role_permission`;
CREATE TABLE `p_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for p_scheduled_task
-- ----------------------------
DROP TABLE IF EXISTS `p_scheduled_task`;
CREATE TABLE `p_scheduled_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `init_start_flag` int(11) DEFAULT NULL,
  `task_cron` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `task_desc` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `task_key` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for p_tag
-- ----------------------------
DROP TABLE IF EXISTS `p_tag`;
CREATE TABLE `p_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(256) NOT NULL COMMENT '标签名',
  `color` varchar(128) DEFAULT NULL COMMENT '颜色',
  `thumbnail` varchar(256) DEFAULT NULL COMMENT '缩略图',
  `slug` varchar(128) DEFAULT NULL COMMENT 'slug',
  `userId` int(11) NOT NULL COMMENT '添加人',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for p_user
-- ----------------------------
DROP TABLE IF EXISTS `p_user`;
CREATE TABLE `p_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account` varchar(32) NOT NULL COMMENT '账户',
  `userName` varchar(32) NOT NULL COMMENT '账户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `salt` varchar(32) NOT NULL COMMENT '盐值',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态:0正常,1禁用',
  `avatar` varchar(256) DEFAULT NULL COMMENT '头像',
  `roleId` int(11) NOT NULL COMMENT '角色id',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `website` varchar(256) DEFAULT NULL COMMENT '网站地址',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `lastLoginTime` datetime DEFAULT NULL  COMMENT '最后登录时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `account` (`account`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `p_word`;
CREATE TABLE `p_word` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '美句',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
