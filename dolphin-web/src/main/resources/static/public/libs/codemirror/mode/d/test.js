// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

(function() {
  var mode = CodeMirror.getMode({indentUnit: 2}, "d");
  function MT(name) { test.mode(name, mode, Array.prototype.slice.call(arguments, 1)); }

  MT("nested_comments",
     "[dolphin-comment /+]","[dolphin-comment dolphin-comment]","[dolphin-comment +/]","[variable void] [variable main](){}");

})();
