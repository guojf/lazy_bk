<p align="center">
    <a href="http://dolphin.org.cn" target="_blank" rel="noopener noreferrer">
        <img width="100" src="/screenshot/logo.png" alt="dolphinBlog logo" />
    </a>
</p>

> dolphinBlog是一款基于java开发的博客建站平台

<p align="center">
  
</p>

------------------------------

## 简介
dolphinBlog3.0是一款基于Java SpringBoot开发的博客建站平台,支持多主题及扩展插件功能,给您带来全新的创作体验，把这个
2.0的模块分成改成插件方式进行集成

## 注意
不用任何数据库配置，在程序跑起来时候，打开首页时，会进入如下页面
![安装](./screenshot/安装.png)
## PR
由于个人维护精力有限，可能升级功能比较慢，如果你有能力优化或者升级，你可以帮忙优化后请求PR合并。
## 功能简介
### 2022-10-25：
* 增加美句功能（当然也支持api获取，在 设置-》其他设置-》个性签名）

![美句](./screenshot/美句.png)
### 2022-10-22：
* 增加背景-》桌面右键菜单-》切换桌面背景-》背景

![背景](./screenshot/背景.png)
### 2022-10-19：
* 优化dolphin主题
* 优化后台图片选择
### 2022-10-18：
* 优化dolphin主题
* 增加自定义body，放置于前台每个页面的body标签中,通常可以用来添加音乐插件等
![音乐](./screenshot/音乐.png)
![音乐](./screenshot/body.png)
### 2022-10-16：
* 优化dolphin主题
* 增加相册模块
### 2022-10-15：
* 增加dolphin主题
* 优化菜单，每个主题单独对应的菜单管理
* 增加最后登录时间
* 优化主题编辑问题，可以在线修改主题

### 第一版本：
* 支持动态的权限-角色-人员-菜单的设置
* 支持系统资源监控，行为日志等监控
* 后台系统采用win10-ui，去除传统的后台样式
* 采用[Markdown](https://www.markdownguide.org/)/富文本双编辑器,支持一键插入视频、图片、附件等
* 支持多主题自由切换
* 强大的主题在线编辑功能,支持多种文件操作,实时生效,支持在网页直接创建新主题,快速开发页面
* 支持自定义页面
* 支持数据库备份
* 支持定时任务调度
* 支持友情链接
* 支持附件管理,附件在线预览，相册管理
* 支持扩展插件,编写java代码打包为jar,直接后台安装生效,丰富程序多样性
* 主题开发简单快速,多种自定义指令及api接口支持
* 支持邮件服务,评论信息邮件提醒
* 安装部署简单


## 快速开始
windows/linux:
```
下载最新的 dolphinBlog 运行包解压后执行:
windows: 双击start.bat
linux: ./start.sh start
```
docker:
```
docker run --name dolphin -d -p 8080:8080 dolphin/dolphin
```


## 后台界面
### 写文章
![写文章](./screenshot/写文章.png)

### 定时任务
![主题](./screenshot/定时任务.png)

### 权限管理
![主题编辑](./screenshot/权限管理.png)

### 附件管理
![主题编辑](./screenshot/附件管理.png)

### 内容管理
![内容编管理](./screenshot/内容管理.png)

### 主题
![主题](./screenshot/主题.png)

### 插件
![插件](./screenshot/插件.png)

### 数据库备份
![数据库备份](./screenshot/数据库备份.png)
## 技术栈
以下包含了所使用到的技术栈及开发环境版本

| 名称                 | 版本     |
|--------------------|--------|
| JDK                | 1.8    |
| Mysql              | 8.0.21 |
| SpringBoot         | 2.5.14 |
| Mybatis-SpringBoot | 2.2.2  |
| Enjoy              | 4.9.02 |
| shiro              | 1.9.1  |

## 源码运行
如需使用源码运行或二次开发,可参考以下步骤:
```
注意: jdk版本为1.8
1. 下载源码并在idea打开
2. 执行maven clean
3. 执行maven install
4. 运行dolphin-web下的Application.java即可(不需要配置数据库,不需要配置数据库)
5. 访问8080端口进行安装
```
打包注意事项:
```
打包前需将dolphin-web模块pom.xml中注释的部分放开(不然模板引擎会使用jar包里的模板)
<excludes>
    <exclude>static/**</exclude>
    <exclude>dolphin.sql</exclude>
</excludes>
<filtering>true</filtering>

之后执行maven clean, maven packge即可,打包后会生成zip,tar.gz压缩包请选择任意一个使用,不要单独使用jar包
```
## 在线文档说明

https://www.wubaobaotools.cn/article/5

## demo查看
https://www.wubaobaotools.cn/login
账号/密码：test/testtest

