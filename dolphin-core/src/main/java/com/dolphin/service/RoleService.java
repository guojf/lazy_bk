package com.dolphin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dolphin.commons.Pager;
import com.dolphin.model.Role;

import java.util.List;

/**
 * @author dolphin
 * @description RoleService
 * @date 2021/11/15 10:24
 */
public interface RoleService extends IService<Role> {

    /**
     * 获取角色列表(不分页)
     *
     * @return List<Role>
     */
    List<Role> getRoleList();

    Pager<Role> list(Pager<Role> pager);

    /**
     * 根据角色代码获取角色
     *
     * @param roleCode 角色代码
     * @return Role
     */
    Role getRoleByCode(String roleCode);

    /**
     * 根据角色代码获取角色
     *
     * @param id 角色代码
     * @return Role
     */
    Role getRoleById(Long id);

    int add(Role role);

}
