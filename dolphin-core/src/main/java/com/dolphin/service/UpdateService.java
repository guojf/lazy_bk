package com.dolphin.service;

import com.dolphin.commons.Update;

/**
 * @description UpdateService
 * @author dolphin
 * @date 2021/11/15 10:30
 */
public interface UpdateService {

    /**
     * @description 检测是否有更新
     * @author dolphin
     * @date 2021/10/29 15:01
     */
    Update checkUpdate();
}
