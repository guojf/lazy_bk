package com.dolphin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dolphin.commons.Pager;
import com.dolphin.model.DataBaseBackup;

import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-05 23:00
 * 个人博客地址：https://www.nonelonely.com
 */
public interface DataBaseBackupService extends IService<DataBaseBackup> {

    /**
     * 查询所有备份数据
     */
    List<DataBaseBackup> selectBackupsList();

    /**
     * mysql备份接口
     */
    Object mysqlBackups() throws Exception;

    /**
     * 根据ID查询
     */
    DataBaseBackup selectListId(Long id);

    /**
     * 恢复数据库
     *
     * @param smb 恢复对象
     * @return
     */
    Object rollback(DataBaseBackup smb) throws Exception;

    Pager<DataBaseBackup> list(Pager<DataBaseBackup> pager);
}
